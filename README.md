# JSON Handling Task
## Task Outline
- Import JSON module
- Open and read a JSON file
- Using the data, give the user the exchange rate of the desired currency
- The data returned should follow an easy-to-read format
## Pseudo-code
```
- Prompt the user for the file name
- In a function:
    - Check if the file name is correct
    - Relay information back to the user
- Prompt the user for a currency code
- In a function:
    - Open json file
    - Read json file
    - Convert information into a dict
    - Use user input to find the rate of their selected currency
    - Return rate back to the user
```
## Task Solution
### In the exchange_rate_parser.py file

```python
import json  # Importing JSON module


class Exchange:  # Creating class for methods and attributes

    def __init__(self):  # Adding attributes so that all methods are able to close a while loop
        self.user_prompt1 = True
        self.user_prompt2 = True

    def File_Check(self):  # Creating a method to check for the exchange rates file
        self.file_name = str(input("What is the name of the exchange rate file "  # Prompting the user for file name
                                   "(or type 'exit' to close the program)?  "))
        try:  # Opening the file, if it succeeds, the the while loop is closed
            open(self.file_name, "r")
            self.user_prompt1 = False
            return "Exchange rates are available"
        except FileNotFoundError:
            if self.file_name.upper() == "EXIT":  # If the user wants to exit, both while loops are closed
                self.user_prompt1 = False
                self.user_prompt2 = False
            else:
                return "\nThe exchange rates could not be acquired, Try again."

    def Exchange_Rate(self):
        self.currency = str(input("Please enter the 3 character code for the currency you would like to exchange or "
                                  "type 'exit' to close the program:  "))  # Prompting the user for a currency
        with open(self.file_name, "r") as file:  # Opening and storing the .json data into a python dict
            self.exchange_rates_dict = json.load(file)
        try:
            exchange_rates = self.exchange_rates_dict["rates"]  
            # Using the data from the dict and only keeping the 'rates' dict 
            return f"The exchange rate for EUR to {self.currency.upper()} is {exchange_rates[self.currency.upper()]}"
            # Returning the exchange rate back to the user
        except KeyError: # If the key inputted by the user is not in the dict
            if self.currency.upper() == "EXIT":  # If the input was 'exit' then the while loop is closed
                self.user_prompt2 = False
                return "Thank you for using this program"
            else:  # If the key given is invalid, the loop repeats
                return "That currency code is INVALID, please try again:"
```

### In the program.py file
```python
from app.exchange_rate_parser import Exchange  # Importing the class from the functional .py file

ex = Exchange()
while ex.user_prompt1:  # Creating a while loop using an attribute
    print(ex.File_Check())
while ex.user_prompt2:  # Creating a while loop using another attribute
    print(ex.Exchange_Rate())
```
Because all the functionality is in the methods of the class, the program.py file is clean, and the code is not cluttered