from setuptools import setup

setup(name="app")
version = "1.0"
description = "Currency exchange display"
author = "Raymond Mo at Sparta Global"
author_email = "Raymond.WL.Mo@gmail.com"
url = "https://spartaglobal.com"
